# React Tools for Development

## Note: This is deprecated.  See [react-agent](https://gitlab.com/simoncomputing-public/jenkins-agents) and [aws-agent](https://gitlab.com/simoncomputing-public/jenkins-agents/aws-agent) for the later versions.

This is an image that can be used for React development as well as deployment to AWS.  This image is based
on Alpine and includes NodeJS, NPM, Yarn, Python and the AWS command line tools.

You'll be able to build your project and deploy it to AWS S3 from this image. 

## Prerequisite

Docker is installed on your machine: https://store.docker.com/editions/community/docker-ce-desktop-mac   

## Developer Installation

```shell
# Get the image
docker pull simoncomputing/react-tools

The images are tagged with a date, so you can pull a specific image, which recommended: 

docker pull simoncomputing/react-tools:2018-11-02

# Create an alias to simplify running the docker command:
alias react-tools='docker run -it -w /react --mount type=bind,source="$(pwd)",target=/react react-tools'

# You can insert this into your **~/.bash_profile**, allowing you to simply change directory into your project directory and type ```react-tools``` to begin building with it.

```

## Usage

You can search for specific dated snapshots here: https://hub.docker.com/r/simoncomputing/react-tools/tags/

Now all you have to do is cd into your working directory and type ```react-tools```.  You're React Docker instance will be created and the /react directory will be mapped to the directory you started in.   From there you can run ```npm``` and ```yarn``` commands and it will run in the project directory you started in.

Distribute these instructions to your team and you will be able to have everyone running on the same set of tools.  Use it for Jenkins and it will be building with the same tools the development teams are.

## Jenkins Integration

In the Jenkinsfile, you can specify this Docker image as the agent running the build step.  See the following syntax: 

```shell
pipeline {
    agent any
    stages {
        stage('Login') {
            steps {
                cleanWs()
                sh '$(aws ecr get-login --no-include-email --region us-east-1)'
            }
        }
        stage('Build') {
            agent {
               docker {
                   image 'simoncomputing/react-tools:2020-07-15'
                   reuseNode true
               }
            }

            steps {
                checkout scm
                sh 'yarn'
                sh 'yarn build'
                sh 'yarn test src/modules --coverage'
            }
        }
        stage('Sonar') {
            agent {
               docker {
                   image 'simoncomputing/react-tools:2019-01-10'
                   reuseNode true
               }
            }
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh "yarn sonar-scanner -Dsonar.login=$SONAR_AUTH_TOKEN"
                }
            }

        }
        stage('Deploy') {
            agent {
               docker { 
                   image 'simoncomputing/react-tools:2020-07-15' 
                   args  '-u root'
                   reuseNode true
               }
            }
            steps {
                sh '''
                    yarn deploy
                '''
            }
        }
    }
}

```

## Creating a New Build

Create the image

```shell
docker build . -t react-tools

# Go to a build directory for React and run the following 
# command to inspect the contents of the image.
docker run -it -w /react --mount type=bind,source="$(pwd)",target=/react react-tools
docker run -it -w /react --mount type=bind,source="$(pwd)",target=/react simoncomputing/react-tools:2020-07-15

# You can create an alias in your **~/.bash_profile** so you don't have to type so much:
alias react-tools='docker run -it -w /react --mount type=bind,source="$(pwd)",target=/react react-tools
alias react-tools='docker run -it -w /react --mount type=bind,source="$(pwd)",target=/react simoncomputing/react-tools:2020-07-15'
```

## Deploying to DockerHub

Pushing to DockerHub and making it publicly available makes it easier to get the image to both Jenkins and developers in general.

You'll need to get access to the public DockerHub SimonComputing repository.

```shell
docker tag react-tools simoncomputing/react-tools:2020-07-15.05
docker push simoncomputing/react-tools:2020-07-15.05

docker tag react-tools simoncomputing/react-tools:latest
docker push simoncomputing/react-tools:latest
```

## Project Setup for React/TypeScript

Add **sonar-scanner** to project: 

```shell
cd {typescript project directory}
yarn add sonar-scanner
```

Add a sonar-scanner script in **package.json**:

```json
  "scripts": {
    "sonar-scanner": "node_modules/sonar-scanner/bin/sonar-scanner"
  },
```

You can test by manually running this to test:

```shell
yarn sonar-scanner -Dsonar.login=$SONAR_AUTH_TOKEN
```

Get the sonar-token from SonarQube | Administration | User and generate a token off of a user created for Jenkins.

## Version list for 2020-07-15

```shell
Build date: 2020-07-15
Build time: 00:41:45
git version 2.20.1
Docker version 19.03.12, build 48a66213fe
NVM 0.35.3
Node v14.5.0
NPM 6.14.5
Yarn 1.22.4
Python 3.7.3
pip 18.1 from /usr/lib/python3/dist-packages/pip (python 3.7)
aws-cli/1.18.97 Python/3.7.3 Linux/4.9.184-linuxkit botocore/1.17.20
```

## References

1. https://github.com/jenkinsci/docker-inbound-agent
1. https://docs.docker.com/engine/install/debian/
1. https://github.com/nvm-sh/nvm/blob/master/README.md#install--update-script
1. https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner
